FROM node:current-alpine as builder

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app

COPY package*.json tsconfig.json ./
COPY . .

RUN npm ci
RUN npm install @nestjs/cli -g
RUN npm run зібрати

FROM node:current-alpine

WORKDIR /usr/src/app

COPY package*.json ./
COPY prisma ./

RUN npm ci --omit=dev

COPY --from=builder /usr/src/app/збірка ./збірка

EXPOSE 3000

CMD [ "npm", "run", "пуск:чисте" ]