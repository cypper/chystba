-- CreateTable
CREATE TABLE "Slovo" (
    "slovo" TEXT NOT NULL,
    "zrazokId" TEXT,

    CONSTRAINT "Slovo_pkey" PRIMARY KEY ("slovo")
);

-- CreateTable
CREATE TABLE "Vidmina" (
    "id" UUID NOT NULL,
    "vidmina" TEXT NOT NULL,
    "chastynaMovy" TEXT NOT NULL,
    "rid" TEXT,
    "chyslo" TEXT,
    "vidminok" TEXT,
    "uzirDiyeslova" TEXT,
    "vydDiyeslova" TEXT,
    "uzirPryslivnyka" TEXT,
    "vydDiyepryslivnyka" TEXT,
    "osoba" TEXT,
    "verstva" TEXT,
    "vydZaimennyka" TEXT,
    "slovoId" TEXT NOT NULL,

    CONSTRAINT "Vidmina_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Pereklad" (
    "id" TEXT NOT NULL,
    "pervyneSlovo" TEXT NOT NULL,
    "pervyneSlovoRid" TEXT,
    "pervyneSlovoChM" TEXT,
    "perekladeneSlovo" TEXT NOT NULL,
    "perekladeneSlovoRid" TEXT,
    "perekladeneSlovoChM" TEXT,
    "perekladeneSlovoZrazok" TEXT,

    CONSTRAINT "Pereklad_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE INDEX "Slovo_slovo_idx" ON "Slovo"("slovo");

-- CreateIndex
CREATE INDEX "Vidmina_vidmina_idx" ON "Vidmina"("vidmina");

-- CreateIndex
CREATE INDEX "Vidmina_slovoId_idx" ON "Vidmina"("slovoId");

-- CreateIndex
CREATE INDEX "Pereklad_pervyneSlovo_idx" ON "Pereklad"("pervyneSlovo");

-- AddForeignKey
ALTER TABLE "Slovo" ADD CONSTRAINT "Slovo_zrazokId_fkey" FOREIGN KEY ("zrazokId") REFERENCES "Slovo"("slovo") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Vidmina" ADD CONSTRAINT "Vidmina_slovoId_fkey" FOREIGN KEY ("slovoId") REFERENCES "Slovo"("slovo") ON DELETE RESTRICT ON UPDATE CASCADE;
