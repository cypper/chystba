/*
  Warnings:

  - You are about to drop the column `vydImennyka` on the `Vidmina` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "Vidmina" DROP COLUMN "vydImennyka",
ADD COLUMN     "zhyvynaImennyka" TEXT;

-- CreateTable
CREATE TABLE "OznakaSlova" (
    "id" TEXT NOT NULL,

    CONSTRAINT "OznakaSlova_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "_OznakaSlovaToVidmina" (
    "A" TEXT NOT NULL,
    "B" UUID NOT NULL
);

-- CreateIndex
CREATE UNIQUE INDEX "_OznakaSlovaToVidmina_AB_unique" ON "_OznakaSlovaToVidmina"("A", "B");

-- CreateIndex
CREATE INDEX "_OznakaSlovaToVidmina_B_index" ON "_OznakaSlovaToVidmina"("B");

-- AddForeignKey
ALTER TABLE "_OznakaSlovaToVidmina" ADD CONSTRAINT "_OznakaSlovaToVidmina_A_fkey" FOREIGN KEY ("A") REFERENCES "OznakaSlova"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_OznakaSlovaToVidmina" ADD CONSTRAINT "_OznakaSlovaToVidmina_B_fkey" FOREIGN KEY ("B") REFERENCES "Vidmina"("id") ON DELETE CASCADE ON UPDATE CASCADE;
