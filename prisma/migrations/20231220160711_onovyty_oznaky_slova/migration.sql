/*
  Warnings:

  - The primary key for the `OznakaSlova` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - You are about to drop the column `id` on the `OznakaSlova` table. All the data in the column will be lost.
  - Added the required column `oznaka` to the `OznakaSlova` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE "_OznakaSlovaToVidmina" DROP CONSTRAINT "_OznakaSlovaToVidmina_A_fkey";

-- AlterTable
ALTER TABLE "OznakaSlova" DROP CONSTRAINT "OznakaSlova_pkey",
DROP COLUMN "id",
ADD COLUMN     "oznaka" TEXT NOT NULL,
ADD CONSTRAINT "OznakaSlova_pkey" PRIMARY KEY ("oznaka");

-- AddForeignKey
ALTER TABLE "_OznakaSlovaToVidmina" ADD CONSTRAINT "_OznakaSlovaToVidmina_A_fkey" FOREIGN KEY ("A") REFERENCES "OznakaSlova"("oznaka") ON DELETE CASCADE ON UPDATE CASCADE;
