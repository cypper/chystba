/*
  Warnings:

  - You are about to drop the `OznakaSlova` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `_OznakaSlovaToVidmina` table. If the table is not empty, all the data it contains will be lost.
  - Added the required column `oznaky` to the `Vidmina` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE "_OznakaSlovaToVidmina" DROP CONSTRAINT "_OznakaSlovaToVidmina_A_fkey";

-- DropForeignKey
ALTER TABLE "_OznakaSlovaToVidmina" DROP CONSTRAINT "_OznakaSlovaToVidmina_B_fkey";

-- AlterTable
ALTER TABLE "Vidmina" ADD COLUMN     "oznaky" TEXT NOT NULL;

-- DropTable
DROP TABLE "OznakaSlova";

-- DropTable
DROP TABLE "_OznakaSlovaToVidmina";
