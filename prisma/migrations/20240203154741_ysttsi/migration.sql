-- CreateTable
CREATE TABLE "Ystets" (
    "id" UUID NOT NULL,
    "slovo" TEXT NOT NULL,
    "golovnyi" BOOLEAN NOT NULL,
    "statyaId" UUID NOT NULL,

    CONSTRAINT "Ystets_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "StatyaYstetsya" (
    "id" UUID NOT NULL,
    "posylannya" TEXT NOT NULL,

    CONSTRAINT "StatyaYstetsya_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "YstYstetsya" (
    "id" UUID NOT NULL,
    "yst" TEXT NOT NULL,
    "statyaId" UUID NOT NULL,

    CONSTRAINT "YstYstetsya_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "Ystets" ADD CONSTRAINT "Ystets_statyaId_fkey" FOREIGN KEY ("statyaId") REFERENCES "StatyaYstetsya"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "YstYstetsya" ADD CONSTRAINT "YstYstetsya_statyaId_fkey" FOREIGN KEY ("statyaId") REFERENCES "StatyaYstetsya"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
