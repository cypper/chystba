/*
  Warnings:

  - Added the required column `poryadkoveChyslo` to the `YstYstetsya` table without a default value. This is not possible if the table is not empty.
  - Added the required column `poryadkoveChyslo` to the `Ystets` table without a default value. This is not possible if the table is not empty.
  - Added the required column `poryadkoveChysloVerstvy` to the `Ystets` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "YstYstetsya" ADD COLUMN     "poryadkoveChyslo" INTEGER NOT NULL;

-- AlterTable
ALTER TABLE "Ystets" ADD COLUMN     "poryadkoveChyslo" INTEGER NOT NULL,
ADD COLUMN     "poryadkoveChysloVerstvy" INTEGER NOT NULL;
