-- CreateTable
CREATE TABLE "StriiVidminy" (
    "spoluchnykyOsnov" TEXT NOT NULL,
    "zakinchennya" TEXT NOT NULL,
    "pislyarostky" TEXT NOT NULL,
    "vidminaId" UUID NOT NULL,

    CONSTRAINT "StriiVidminy_pkey" PRIMARY KEY ("vidminaId")
);

-- CreateTable
CREATE TABLE "StriiOsnovyVidminy" (
    "id" UUID NOT NULL,
    "vkaz" INTEGER NOT NULL,
    "striiVidminyId" UUID NOT NULL,
    "striiOsnovyId" TEXT NOT NULL,

    CONSTRAINT "StriiOsnovyVidminy_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "StriiOsnovy" (
    "osnova" TEXT NOT NULL,
    "pryrostky" TEXT NOT NULL,
    "korin" TEXT NOT NULL,
    "narostky" TEXT NOT NULL,

    CONSTRAINT "StriiOsnovy_pkey" PRIMARY KEY ("osnova")
);

-- AddForeignKey
ALTER TABLE "StriiVidminy" ADD CONSTRAINT "StriiVidminy_vidminaId_fkey" FOREIGN KEY ("vidminaId") REFERENCES "Vidmina"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "StriiOsnovyVidminy" ADD CONSTRAINT "StriiOsnovyVidminy_striiVidminyId_fkey" FOREIGN KEY ("striiVidminyId") REFERENCES "StriiVidminy"("vidminaId") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "StriiOsnovyVidminy" ADD CONSTRAINT "StriiOsnovyVidminy_striiOsnovyId_fkey" FOREIGN KEY ("striiOsnovyId") REFERENCES "StriiOsnovy"("osnova") ON DELETE RESTRICT ON UPDATE CASCADE;
