/*
  Warnings:

  - The primary key for the `StriiOsnovy` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - You are about to drop the `StriiOsnovyVidminy` table. If the table is not empty, all the data it contains will be lost.
  - The required column `id` was added to the `StriiOsnovy` table with a prisma-level default value. This is not possible if the table is not empty. Please add this column as optional, then populate it before making it required.
  - Added the required column `striiVidminyId` to the `StriiOsnovy` table without a default value. This is not possible if the table is not empty.
  - Added the required column `vkaz` to the `StriiOsnovy` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE "StriiOsnovyVidminy" DROP CONSTRAINT "StriiOsnovyVidminy_striiOsnovyId_fkey";

-- DropForeignKey
ALTER TABLE "StriiOsnovyVidminy" DROP CONSTRAINT "StriiOsnovyVidminy_striiVidminyId_fkey";

-- AlterTable
ALTER TABLE "StriiOsnovy" DROP CONSTRAINT "StriiOsnovy_pkey",
ADD COLUMN     "id" UUID NOT NULL,
ADD COLUMN     "striiVidminyId" UUID NOT NULL,
ADD COLUMN     "vkaz" INTEGER NOT NULL,
ADD CONSTRAINT "StriiOsnovy_pkey" PRIMARY KEY ("id");

-- DropTable
DROP TABLE "StriiOsnovyVidminy";

-- AddForeignKey
ALTER TABLE "StriiOsnovy" ADD CONSTRAINT "StriiOsnovy_striiVidminyId_fkey" FOREIGN KEY ("striiVidminyId") REFERENCES "StriiVidminy"("vidminaId") ON DELETE RESTRICT ON UPDATE CASCADE;
