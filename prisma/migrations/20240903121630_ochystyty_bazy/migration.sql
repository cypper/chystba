/*
  Warnings:

  - You are about to drop the `Slovo` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `StatyaYstetsya` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `StriiOsnovy` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `StriiVidminy` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `Vidmina` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `YstYstetsya` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `Ystets` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE "Slovo" DROP CONSTRAINT "Slovo_zrazokId_fkey";

-- DropForeignKey
ALTER TABLE "StriiOsnovy" DROP CONSTRAINT "StriiOsnovy_striiVidminyId_fkey";

-- DropForeignKey
ALTER TABLE "StriiVidminy" DROP CONSTRAINT "StriiVidminy_vidminaId_fkey";

-- DropForeignKey
ALTER TABLE "Vidmina" DROP CONSTRAINT "Vidmina_slovoId_fkey";

-- DropForeignKey
ALTER TABLE "YstYstetsya" DROP CONSTRAINT "YstYstetsya_statyaId_fkey";

-- DropForeignKey
ALTER TABLE "Ystets" DROP CONSTRAINT "Ystets_statyaId_fkey";

-- DropTable
DROP TABLE "Slovo";

-- DropTable
DROP TABLE "StatyaYstetsya";

-- DropTable
DROP TABLE "StriiOsnovy";

-- DropTable
DROP TABLE "StriiVidminy";

-- DropTable
DROP TABLE "Vidmina";

-- DropTable
DROP TABLE "YstYstetsya";

-- DropTable
DROP TABLE "Ystets";
