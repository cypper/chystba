import { Module } from '@nestjs/common';
import { ПострійВузол } from '../пострій/пострій.вузол.js';
import { ПризмаСлужба } from './призма.служба.js';

@Module({
  imports: [ПострійВузол],
  providers: [ПризмаСлужба],
  exports: [ПризмаСлужба],
})
export class ПризмаВузол {}
