import { Injectable, OnModuleInit } from '@nestjs/common';
import { PrismaClient } from '@prisma/client';
import { ПострійСлужба } from '../пострій/пострій.служба.js';

@Injectable()
export class ПризмаСлужба extends PrismaClient implements OnModuleInit {
  constructor(private readonly пострійСлужба: ПострійСлужба) {
    super({
      log: пострійСлужба.getPrismaLogLevels(),
      errorFormat: 'pretty',
    });
  }

  async onModuleInit() {
    await this.$connect();
  }
}
