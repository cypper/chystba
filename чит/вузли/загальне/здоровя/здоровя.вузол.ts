import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { TerminusModule } from '@nestjs/terminus';
import { ПострійВузол } from '../пострій/пострій.вузол.js';
import { ПризмаВузол } from '../бд/призма.вузол.js';
import { ЗдоровяКін } from './здоровя.кін.js';
import { PrismaHealthIndicator } from './призма.здоровя.js';

@Module({
  imports: [HttpModule, ПострійВузол, TerminusModule, ПризмаВузол],
  providers: [ЗдоровяКін, PrismaHealthIndicator],
})
export class ЗдоровяВузол {}
