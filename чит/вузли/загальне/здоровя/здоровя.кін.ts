import {
  HealthCheck,
  HealthCheckService,
  HttpHealthIndicator,
  MemoryHealthIndicator,
} from '@nestjs/terminus';
import { Controller, Get } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { ПострійСлужба } from '../пострій/пострій.служба.js';
import { PrismaHealthIndicator } from './призма.здоровя.js';

@ApiTags('Здоровя')
@Controller({ path: 'здоровя' })
export class ЗдоровяКін {
  constructor(
    private readonly пострій: ПострійСлужба,
    private readonly db: PrismaHealthIndicator,
    private readonly http: HttpHealthIndicator,
    private readonly health: HealthCheckService,
    private readonly memory: MemoryHealthIndicator,
  ) {}

  /* Check server health in case of constant internal server errors */
  @Get()
  @HealthCheck()
  async здоровя(): Promise<boolean> {
    const { serverUrl } = this.пострій.getHttp();

    const health = await this.health.check([
      async () =>
        this.http.pingCheck('присвідчення', `${serverUrl}/присвідчення`),
      async () => this.db.isHealthy('database'),
      async () => this.memory.checkHeap('memory_heap', 200 * 1_024 * 1_024),
      async () => this.memory.checkRSS('memory_rss', 3_000 * 1_024 * 1_024),
    ]);

    return health.status === 'ok';
  }
}
