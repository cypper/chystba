import { Injectable } from '@nestjs/common';
import {
  HealthCheckError,
  HealthIndicator,
  HealthIndicatorResult,
} from '@nestjs/terminus';
import { ПризмаСлужба } from '../бд/призма.служба.js';

@Injectable()
export class PrismaHealthIndicator extends HealthIndicator {
  constructor(private readonly призмаСлужба: ПризмаСлужба) {
    super();
  }

  async isHealthy(key: string): Promise<HealthIndicatorResult> {
    try {
      await this.призмаСлужба.$queryRaw`SELECT 1`;
      return this.getStatus(key, true);
    } catch (e) {
      throw new HealthCheckError('Prisma check failed', e);
    }
  }
}
