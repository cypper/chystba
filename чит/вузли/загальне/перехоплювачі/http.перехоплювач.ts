import { Observable, throwError } from 'rxjs';
import { catchError, finalize } from 'rxjs/operators';
import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
  HttpException,
  Logger,
  HttpStatus,
} from '@nestjs/common';
import { Response } from 'express';
import { ПострійСлужба } from '../пострій/пострій.служба.js';

@Injectable()
export class HttpПерехоплювач implements NestInterceptor {
  private readonly logger = new Logger(HttpПерехоплювач.name);

  constructor(private readonly пострійСлужба: ПострійСлужба) {}

  intercept(
    context: ExecutionContext,
    next: CallHandler,
  ): Observable<Response> {
    const now = Date.now();
    return next.handle().pipe(
      catchError((error) =>
        throwError(() => {
          if (error instanceof HttpException) {
            this.logger.error(error.stack, error.getResponse());

            throw error;
          } else {
            this.logger.error(error.stack, JSON.stringify(error, null, 2));

            throw new HttpException(
              error.message,
              HttpStatus.INTERNAL_SERVER_ERROR,
              { cause: error },
            );
          }
        }),
      ),
      finalize(() => {
        this.logger.debug(
          `${context.getType()}: [${context.getClass().name}.${
            context.getHandler().name
          }] ${Date.now() - now}ms`,
        );
      }),
    );
  }
}
