import { Module } from '@nestjs/common';
import { ПострійСлужба } from './пострій.служба.js';

@Module({
  imports: [ПострійВузол],
  exports: [ПострійСлужба],
  providers: [
    {
      provide: ПострійСлужба,
      useFactory: () => {
        const пострійСлужба = new ПострійСлужба();
        пострійСлужба.parseAndValidate();
        return пострійСлужба;
      },
    },
  ],
})
export class ПострійВузол {}
