import * as dotenv from 'dotenv';
import { Injectable, LogLevel as NestLogLevel, Logger } from '@nestjs/common';
import { ConfigService as ConfService } from '@nestjs/config';
import { Prisma } from '@prisma/client';

dotenv.config();

@Injectable()
export class ПострійСлужба extends ConfService<Record<string, unknown>, true> {
  private readonly logger = new Logger(ПострійСлужба.name);

  public parseAndValidate() {
    if (!this.get('DATABASE_URL'))
      throw new Error('Database url must be provided');

    if (!this.get('TMP_FOLDER'))
      throw new Error('Тимчасова папка повинна бути');

    if (!this.get('HTTP_PORT') || !this.get('HTTP_SERVER_URL'))
      throw new Error('Http config must be set');
  }

  public getHttp() {
    return {
      port: this.get<number>('HTTP_PORT', 3000),
      serverUrl: this.get<string>('HTTP_SERVER_URL'),
    };
  }

  getDatabaseUrl() {
    return this.get('DATABASE_URL');
  }

  getPrismaLogLevels(): Prisma.LogLevel[] {
    const prismaMap: Record<NestLogLevel, Prisma.LogLevel> = {
      log: 'info',
      warn: 'warn',
      fatal: 'error',
      error: 'error',
      debug: 'info',
      verbose: 'query',
    };
    const logLevels = this.рівніСлівності().filter((l) =>
      Object.keys(prismaMap).includes(l),
    );

    return logLevels.map((l) => prismaMap[l]);
  }

  чиБагатослівно() {
    return this.рівніСлівності().includes('verbose');
  }

  рівніСлівності(): NestLogLevel[] {
    return (this.get<string | undefined>('LOG_LEVELS') ?? '')
      .split(',')
      .map((l) => l.trim()) as NestLogLevel[];
  }

  тимчПапка() {
    return this.get('TMP_FOLDER');
  }
}
