import { Prisma } from '@prisma/client';
import {
  TOperationFn,
  TransactionClient,
} from '../../../загальне/transactions/client.js';
import { ПризмаСлужба } from '../бд/призма.служба.js';

export interface ITransactionOptions {
  options?: {
    maxWait?: number;
    timeout?: number;
    isolationLevel?: Prisma.TransactionIsolationLevel;
  };
  tC?: TransactionClient;
}

export abstract class ВідлучнаСлужба {
  protected abstract readonly призмаСлужба: ПризмаСлужба;

  async $transaction<T>(
    operation: TOperationFn<T>,
    opts?: ITransactionOptions,
  ): Promise<T> {
    if (opts?.tC) return operation(opts.tC);
    else {
      return this.призмаСлужба.$transaction(async (tx) => {
        const tC = new TransactionClient(tx);
        return operation(tC).catch(async (err) => {
          await tC.revertAll();
          throw err;
        });
      }, opts?.options);
    }
  }
}
