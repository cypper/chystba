import { Module } from '@nestjs/common';
import { ПисьмоКін } from './письмо/письмо.кін.js';
import { СловоКін } from './слово/слово.кін.js';

@Module({
  imports: [],
  controllers: [ПисьмоКін, СловоКін],
  providers: [],
  exports: [],
})
export class МіжграньВузол {}
