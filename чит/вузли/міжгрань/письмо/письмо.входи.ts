import { ApiProperty } from '@nestjs/swagger';
import { РозбірПисьма, РозбірРечення, РозбірСлова } from '@movnia/spilne';
import { IsOptional, IsString } from 'class-validator';

export class ЦідилаРозборівПисьмаВхід {
  @ApiProperty({
    type: 'string',
    isArray: true,
    enum: [
      'слово',
      'первцевийРС',
      'словозміннийРС',
      'тваровийРС',
      'додатковийРС',
    ] as (keyof РозбірСлова)[],
  })
  @IsString({ each: true })
  @IsOptional()
  ключіРС: (keyof РозбірСлова)[];

  @ApiProperty({
    type: 'string',
    isArray: true,
    enum: [
      'речення',
      'первцевийРР',
      'ладиннийРР',
      'первцевоЩурбнийРР',
      'чолоннийРР',
      'слівнийРР',
      'додатковийРР',
    ] as (keyof РозбірРечення)[],
  })
  @IsString({ each: true })
  @IsOptional()
  ключіРР: (keyof РозбірРечення)[];

  @ApiProperty({
    type: 'string',
    isArray: true,
    enum: ['письмо', 'первцевийРР', 'реченевийРП'] as (keyof РозбірПисьма)[],
  })
  @IsString({ each: true })
  @IsOptional()
  ключіРП: (keyof РозбірПисьма)[];
}

export class ПисьмаВхід {
  @ApiProperty({ type: 'string', isArray: true })
  @IsString({ each: true })
  письма: string[];

  @ApiProperty({ type: ЦідилаРозборівПисьмаВхід, required: false })
  @IsOptional()
  цідила?: ЦідилаРозборівПисьмаВхід;

  @ApiProperty({ type: 'boolean', required: false })
  @IsOptional()
  чиНехтуватиПомилками?: boolean;
}

export class ЙмМережаРеченняJSONВхід {
  @ApiProperty({ type: 'string' })
  @IsString()
  речення: string;
}

export class РозбориПисьмВхід {
  @ApiProperty({ isArray: true })
  @IsString({ each: true })
  розбориПисьм: any[];

  @ApiProperty({ type: ЦідилаРозборівПисьмаВхід, required: false })
  @IsOptional()
  цідила?: ЦідилаРозборівПисьмаВхід;
}
