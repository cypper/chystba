import {
  BadRequestException,
  Body,
  Controller,
  Logger,
  Post,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { однобіжно } from '@movnia/spilne';
import { РозбірПисьма, процідитиРП } from '@movnia/spilne';
import { вПервцевийРП, вРП } from '@movnia/rozbir-pysma';
import { вЙмМережіJSON, вПервцевіРР } from '@movnia/rozbir-rechennia';
import { перекластиРП } from '@movnia/chystba';
import {
  ЙмМережаРеченняJSONВхід,
  ПисьмаВхід,
  РозбориПисьмВхід,
} from './письмо.входи.js';

@Controller(encodeURI('письмо'))
export class ПисьмоКін {
  private readonly logger = new Logger(ПисьмоКін.name);

  @ApiTags('Переклад письм')
  @Post(encodeURI('переклад'))
  async перекладПисьм(
    @Body() { письма, цідила, чиНехтуватиПомилками }: ПисьмаВхід,
  ) {
    return await однобіжно(письма, async (п) => {
      return await this.обробкаПомилок(async () => {
        const рп = await вРП(п, {
          чиВикорПатілоРечення: true,
        });
        const перекладРП = await перекластиРП(рп, {
          чиНехтуватиПомилками,
        });

        return {
          рп: this.процідити(перекладРП.рп, цідила),
          перекладенийРП: this.процідити(перекладРП.перекладенийРП, цідила),
          вказиПерекладівРР: перекладРП.вказиПерекладівРР,
        };
      }, чиНехтуватиПомилками ?? false);
    });
  }

  @ApiTags('Переклад розборів письм')
  @Post(encodeURI('переклад-розбору'))
  async перекладРозборівПисьм(
    @Body() { розбориПисьм, цідила }: РозбориПисьмВхід,
  ) {
    return await однобіжно(розбориПисьм, async (рп) => {
      const перекладРП = await перекластиРП(рп);
      return {
        рп: this.процідити(перекладРП.рп, цідила),
        перекладенийРП: this.процідити(перекладРП.перекладенийРП, цідила),
        вказиПерекладівРР: перекладРП.вказиПерекладівРР,
      };
    });
  }

  @ApiTags('Розбір письм')
  @Post(encodeURI('розбір'))
  async розбірПисьм(
    @Body() { письма, цідила, чиНехтуватиПомилками }: ПисьмаВхід,
  ) {
    return await однобіжно(письма, async (п) => {
      return await this.обробкаПомилок(async () => {
        const рп = await вРП(п);
        return this.процідити(рп, цідила);
      }, чиНехтуватиПомилками ?? false);
    });
  }

  @ApiTags('Речення в йм-мережі json')
  @Post(encodeURI('речення/йм-мережі/json'))
  async ймМережіРеченняJSON(@Body() { речення }: ЙмМережаРеченняJSONВхід) {
    const пРП = await вПервцевийРП(речення);
    if (пРП.length !== 1)
      throw new BadRequestException(
        'Пис містить більше одного речення або жодного',
      );

    const { первцевийРР, первцевоЩурбнийРР } = вПервцевіРР(пРП[0]!.стрічка);

    return вЙмМережіJSON({ первцевийРР, первцевоЩурбнийРР }, 2);
  }

  private async обробкаПомилок<В>(
    дія: () => В,
    чиНехтуватиПомилками: boolean,
  ): Promise<Awaited<В> | null> {
    try {
      return await дія();
    } catch (помилка) {
      if (чиНехтуватиПомилками === true) {
        this.logger.warn(помилка);
        return null;
      } else {
        throw помилка;
      }
    }
  }

  private процідити(рп: РозбірПисьма, цідила?: ПисьмаВхід['цідила']) {
    return цідила
      ? процідитиРП(рп, цідила.ключіРП, цідила.ключіРР, цідила.ключіРС)
      : рп;
  }

  // @ApiTags('Стрій письм')
  // @ApiOkResponse({
  //   type: СтрійПисьмВихід,
  // })
  // @Post(encodeURI('стрій'))
  // async стрій(@Body() { письма }: ПисьмоВхід): Promise<СтрійПисьмВихід> {
  //   const строїПисьм: СтрійРечення[][] = [];
  //   for (const п of письма) {
  //     строїПисьм.push(await this.стрій(п));
  //   }
  //   return { строїПисьм: строїПисьм.map((сП) => ({ строїРечень: сП })) };
  // }
}
