import { Test } from '@nestjs/testing';
import { ПерекладРП } from '@movnia/spilne';
import { ПризмаВузол } from '../../../загальне/бд/призма.вузол.js';
import { СловоКін } from '../../слово/слово.кін.js';
import { ПисьмоКін } from '../письмо.кін.js';

export async function створитиМіжграньВузол() {
  return await Test.createTestingModule({
    imports: [ПризмаВузол],
    controllers: [ПисьмоКін, СловоКін],
    providers: [],
    exports: [],
  }).compile();
}

export function вПерекладенеПисьмо(переклади: (ПерекладРП | null)[]) {
  return переклади.map((п) => п?.перекладенийРП.письмо ?? '');
}

export async function спититиПереклад(
  письмоКін: ПисьмоКін,
  доперекладу: string[],
  післяперекладу: string[],
) {
  expect(
    вПерекладенеПисьмо(
      await письмоКін.перекладПисьм({
        письма: доперекладу,
      }),
    ),
  ).toEqual(післяперекладу);
}
