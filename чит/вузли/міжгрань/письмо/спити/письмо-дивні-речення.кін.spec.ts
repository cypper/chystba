import { TestingModule } from '@nestjs/testing';
import { ПисьмоКін } from '../письмо.кін.js';
import { створитиМіжграньВузол, спититиПереклад } from './допоміжні.js';

describe('ПисьмоКін', () => {
  jest.setTimeout(10000);

  let вузол: TestingModule;
  let письмоКін: ПисьмоКін;

  beforeAll(async () => {
    вузол = await створитиМіжграньВузол();

    письмоКін = вузол.get<ПисьмоКін>(ПисьмоКін);
    await вузол.init();
  });

  afterAll(async () => {
    await вузол.close();
  });

  it('дивні речення', async () => {
    await спититиПереклад(
      письмоКін,
      [
        // TODO: не можливий розбір через наявність кількох підметів
        '#Підготовка населення до ТрО #тероборона #ТериторіальнаОборона #НаціональнийСпротив #ЗСУ #ТрОЗСУя',
        'Умовні познаки: а – у твердий ґрунт; б – у сніг з маскуванням; в – у болотистий ґрунт; 1 – маскуючий ґрунт; 2 – міна; 3 – маскуючий шар снігу; 4 – ущільнений шар снігу; 5 – сніг; 6 – маскуюча дернина; 7 – обсипка ґрунтом; 8 – підкладка з дощок (жердин, хмизу).',
      ],
      [
        '#Підготовка населення до ТрО #тероборона #ТериторіальнаОборона #НаціональнийСпротив #ЗСУ #ТрОЗСУя',
        'Умовні познаки: а – у твердий ґрунт; б – у сніг з личкуванням; в – у болотистий ґрунт; 1 – личкуючий ґрунт; 2 – міна; 3 – личкуючий шар снігу; 4 – ущільнений шар снігу; 5 – сніг; 6 – личкуюча дернина; 7 – обсипка ґрунтом; 8 – підкладка з дощок (жердин, хмизу).',
      ],
    );
  });
});
