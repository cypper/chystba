import { ApiProperty } from '@nestjs/swagger';
import { СтрійОснови, СтрійСлова } from '@movnia/spilne';

export class СтрійОсновиСловаВихід implements СтрійОснови {
  @ApiProperty({ type: 'string', isArray: true })
  передчепи: string[];

  @ApiProperty({ type: 'string' })
  корінь: string;

  @ApiProperty({ type: 'string', isArray: true })
  начепи: string[];

  @ApiProperty({ type: 'string' })
  основа: string;
}

export class СтрійСловаВихід implements СтрійСлова {
  @ApiProperty({ type: СтрійОсновиСловаВихід, isArray: true })
  основи: СтрійОсновиСловаВихід[];

  @ApiProperty({ oneOf: [{ type: 'string' }, { type: 'null' }], isArray: true })
  сполучникиОснов: СтрійСлова['сполучникиОснов'];

  @ApiProperty({ type: 'string' })
  закінчення: string;

  @ApiProperty({ type: 'string', isArray: true })
  післячепи: string[];
}
