import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class СловоВхід {
  @ApiProperty({ type: 'string' })
  @IsString()
  слово: string;
}
