import { BadRequestException, Body, Controller, Post } from '@nestjs/common';
import { ApiExtraModels, ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { вМногоРС, стрійСлова } from '@movnia/rozbir-slova';
import { СловоВхід } from './слово.входи.js';
import { СтрійСловаВихід } from './слово.виходи.js';

@Controller(encodeURI('слово'))
export class СловоКін {
  @ApiTags('Розбори слова')
  @Post(encodeURI('розбори'))
  async розбори(@Body() { слово }: СловоВхід) {
    const розбори = вМногоРС(
      {
        стрічка: слово,
        чиПершеВРеченні: false,
      } /*, { чиСькатиПоПохожих: true }*/,
    );
    return розбори;
  }

  @ApiTags('Стрій слова')
  @ApiExtraModels(СтрійСловаВихід)
  @ApiOkResponse({
    type: СтрійСловаВихід,
  })
  @Post(encodeURI('стрій'))
  async стрій(@Body() { слово }: СловоВхід): Promise<СтрійСловаВихід> {
    const стрій = await стрійСлова(слово);
    if (!стрій) {
      throw new BadRequestException('Нажаль ми не здатні знайти дане слово');
    } else {
      return стрій;
    }
  }
}
