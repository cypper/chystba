import { Prisma } from '@prisma/client';

export type TOperationFn<T> = (tC: TransactionClient) => Promise<T>;
export type TRevertFn = (...args: any[]) => Promise<unknown>;
export class TransactionClient {
  reverts: TRevertFn[] = [];

  constructor(public readonly tx: Prisma.TransactionClient) {}

  addRevert(fn: TRevertFn) {
    this.reverts.push(fn);
  }

  async revertAll() {
    await Promise.all(this.reverts.map((fn) => fn()));
  }
}
