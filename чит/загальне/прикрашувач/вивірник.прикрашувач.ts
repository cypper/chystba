import { applyDecorators } from '@nestjs/common';
import { ClassConstructor, Type } from 'class-transformer';
import {
  registerDecorator,
  ValidateNested,
  ValidationArguments,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';

@ValidatorConstraint({ name: 'when' })
export class WhenConstraint implements ValidatorConstraintInterface {
  validate(value: any, args: ValidationArguments) {
    const [when] = args.constraints;
    return when(args.object, value, args);
  }

  defaultMessage(args: ValidationArguments) {
    return `${args.property} when condition is no satisfied`;
  }
}

export function When<T>(
  type: ClassConstructor<T>,
  whenFn: (o: Partial<T>, value?: any, args?: ValidationArguments) => boolean,
  validationOptions?: ValidationOptions,
) {
  return (object: any, propertyName: string) => {
    registerDecorator({
      target: object.constructor,
      propertyName,
      options: validationOptions,
      constraints: [whenFn],
      validator: WhenConstraint,
    });
  };
}

export function Alone<T>(type: ClassConstructor<T>) {
  return When(type, (o, value) => !!value && Object.keys(o).length === 1, {
    message: ({ property }) => `${property} should be only defined alone`,
  });
}

export function DefinedWhen<T, K extends keyof T>(
  type: ClassConstructor<T>,
  whenProperty: K,
  whenValue: T[K],
) {
  return When(
    type,
    (o, value) => {
      return o[whenProperty] === whenValue
        ? value !== undefined && value !== null
        : value === undefined;
    },
    {
      message: ({ property }) =>
        `${property} should be only defined for "${String(
          whenProperty,
        )}" ${whenValue}`,
    },
  );
}

export function DefinedWith<T>(
  type: ClassConstructor<T>,
  ...properties: Array<keyof T>
) {
  return When(
    type,
    (o) =>
      properties.every(
        (p) => (o as T)[p] !== undefined && (o as T)[p] !== null,
      ),
    {
      message: ({ property }) =>
        `${property} can only be defined with [${properties.join(', ')}]`,
    },
  );
}

export function ContainsUnique<T>(
  type: ClassConstructor<T>,
  uniqueValueFn?: (value: any) => any,
) {
  return When(
    type,
    (_, array) => {
      const values = uniqueValueFn ? array.map(uniqueValueFn) : array;
      return new Set(values).size === values.length;
    },
    {
      message: ({ property }) =>
        `${property} should only contain unique values`,
    },
  );
}

type TGreaterThanCompare<T> = keyof T | ((value: any) => any);

type TGreaterThanOptions = {
  transform?: (value?: any) => any;
  orEqual?: boolean;
};

@ValidatorConstraint({ name: 'isGreaterThan' })
export class IsGreaterThanConstraint<T>
  implements ValidatorConstraintInterface
{
  validate(value: any, args: ValidationArguments) {
    const [comparePropertyOrFn, options] = args.constraints as [
      TGreaterThanCompare<T>,
      TGreaterThanOptions,
    ];

    const transform = options?.transform
      ? options.transform
      : (value: any) => value;

    const readyValue = transform(value);
    const readyCompareValue =
      comparePropertyOrFn instanceof Function
        ? comparePropertyOrFn(value)
        : transform((args.object as T)[comparePropertyOrFn]);

    if (options?.orEqual === true) return readyValue >= readyCompareValue;
    else return readyValue > readyCompareValue;
  }

  defaultMessage(args: ValidationArguments) {
    const [comparePropertyOrFn, options] = args.constraints as [
      TGreaterThanCompare<T>,
      TGreaterThanOptions,
    ];

    const compareProperty =
      comparePropertyOrFn instanceof Function
        ? 'value from compare function'
        : String(comparePropertyOrFn);

    if (options?.orEqual === true)
      return `${args.property} should be greater than or equal ${compareProperty}`;
    else return `${args.property} should be greater than ${compareProperty}`;
  }
}

export function IsGreaterThan<T>(
  type: ClassConstructor<T>,
  comparePropertyOrFn: TGreaterThanCompare<T>,
  options?: TGreaterThanOptions,
  validationOptions?: ValidationOptions,
) {
  return (object: any, propertyName: string) => {
    registerDecorator({
      target: object.constructor,
      propertyName,
      options: validationOptions,
      constraints: [comparePropertyOrFn, options],
      validator: IsGreaterThanConstraint<T>,
    });
  };
}

export function Nested(
  typeFunction: Parameters<typeof Type>[0],
  validationOptions?: ValidationOptions,
) {
  return applyDecorators(ValidateNested(validationOptions), Type(typeFunction));
}
