import { execSync } from 'child_process';
import { випадковийСловотвар } from '@movnia/tvarovyi-slovnyk';

const start =
  process.platform == 'darwin'
    ? 'open'
    : process.platform == 'win32'
      ? 'start'
      : 'xdg-open';

export async function випадковеСловоПрипис() {
  while (true) {
    const слово = випадковийСловотвар().слово;

    // eslint-disable-next-line
    console.log(слово);
    execSync(
      `${start} "https://goroh.pp.ua/%D0%A2%D0%BB%D1%83%D0%BC%D0%B0%D1%87%D0%B5%D0%BD%D0%BD%D1%8F/${encodeURI(слово)}"`,
    );
  }
}
