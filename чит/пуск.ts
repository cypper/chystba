import { NestFactory } from '@nestjs/core';
import {
  Logger,
  LogLevel,
  ValidationPipe,
  VersioningType,
} from '@nestjs/common';
import { NextFunction, Request, Response } from 'express';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { HttpПерехоплювач } from './вузли/загальне/перехоплювачі/http.перехоплювач.js';
import { ПострійСлужба } from './вузли/загальне/пострій/пострій.служба.js';
import { СлужникВузол } from './служник.вузол.js';

async function bootstrap() {
  const app = await NestFactory.create(СлужникВузол, {
    // we cannot access ПострійСлужба here
    logger: process.env.LOG_LEVELS?.split(',').map((s) =>
      s.trim(),
    ) as LogLevel[],
  });
  const configService = app.get(ПострійСлужба);
  const { serverUrl, port } = configService.getHttp();

  app.enableCors();

  app.enableVersioning({
    type: VersioningType.URI,
    defaultVersion: '1',
    prefix: encodeURI('міна'),
  });

  const documentBuilder = new DocumentBuilder()
    .setTitle('Чистьба')
    .setDescription('')
    .setVersion('0.0.1')
    .addServer(serverUrl, 'localhost');

  const document = SwaggerModule.createDocument(app, documentBuilder.build());

  SwaggerModule.setup(encodeURI('присвідчення'), app, document);

  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
      forbidNonWhitelisted: true,
    }),
  );

  app.useGlobalInterceptors(new HttpПерехоплювач(configService));

  app.use((req: Request, res: Response, next: NextFunction) => {
    res.removeHeader('x-powered-by');
    next();
  });

  app.enableShutdownHooks();

  await app.listen(port);
  Logger.log(`Listening on ${port}`);
}
bootstrap();
