/* eslint-disable @typescript-eslint/no-var-requires */
// import { spawn } from 'child_process';

// function shПрипис(рать: string, арг: string[]) {
//   const shell = spawn(рать, арг, { stdio: 'inherit' });
//   shell.on('close', (code) => {
//     // eslint-disable-next-line
//     console.log('[shell] terminated :', code);
//   });
// }

const коріньСнутку = process.cwd();
const args = process.argv.slice(2);

const назваПрипису = args[0] ?? '';

const приписиВідбит = new Map([
  [
    'ручити-словник-перекладів',
    () =>
      require('./вузли/переклад/словник/словник-перекладів-в-бд.припис').словникПерекладівВБдПрипис(
        коріньСнутку,
      ),
  ],

  // РУЧИТИ ДАНІ НАВЧАННЯ
  [
    'словозмінення-кр-ручити-дані-навчання',
    () =>
      require('./єдиниці/словозмінення-кр/створення-даних-навчання/ручити-дані-навчання.припис').ручити(
        коріньСнутку,
      ),
  ],

  // ВСЯКЕ
  [
    'випадкове-слово',
    () => require('./приписи/випадкове-слово.припис').випадковеСловоПрипис(),
  ],
]);

приписиВідбит.set('допомога', () => {
  // eslint-disable-next-line
  console.log([...приписиВідбит.entries()].map(([н]) => н).join('\n'));
});

const припис = приписиВідбит.get(назваПрипису);
if (!припис) throw new Error('Невідомий припис служника');

припис();
