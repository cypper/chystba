import tensorflow as tf
import gzip
import shutil
import pandas as pd
from tensorflow_text.tools.wordpiece_vocab import bert_vocab_from_dataset as bert_vocab
from tensorflow_text.tools.wordpiece_vocab import wordpiece_tokenizer_learner_lib

from загальне import (
    ПУТЬ_ТИМЧ_СНІПНИЦІ,
    ЗАПАШЕНІ_ЛИЧМАНИ,
    БЕРТ_СЛОВНИК_ПУТЬ,
    ПУТЬ_ДАНИХ_НАВЧАННЯ,
)

print("-" * 100 + "\n" * 4)

# ПОСИЛАННЯ_НА_ВИКИД = 'https://dumps.wikimedia.org/uawikimedia/latest/uawikimedia-latest-pages-articles-multistream.xml.bz2'
ПОСИЛАННЯ_НА_ВИКИД = "https://lindat.mff.cuni.cz/repository/xmlui/bitstream/handle/11234/1-2735/uk.txt.gz"
вікі_пис_путь = ПУТЬ_ТИМЧ_СНІПНИЦІ / "вікі/вікі_викид.txt"
вікі_тф_путь = ПУТЬ_ТИМЧ_СНІПНИЦІ / "вікі/вікі.tfrecord"

basic_tokenizer_params = dict(lower_case=False)
reserved_tokens = ЗАПАШЕНІ_ЛИЧМАНИ
learn_params = dict()


def ручити_вікі_тф():
    ######
    # для bz2
    # вікі_викид_боріг_путь = tf.keras.utils.get_file("вікі_викид.xml.bz2", ПОСИЛАННЯ_НА_ВИКИД,
    #                                     extract=True, cache_dir=ПУТЬ_ТИМЧ_СНІПНИЦІ,
    #                                     cache_subdir='вікі')

    # вікі_викид_хмл_путь = Path(вікі_викид_боріг_путь).parent/'вікі_викид.xml'
    # if not вікі_викид_хмл_путь.is_file():
    #     with open(вікі_викид_хмл_путь, 'wb') as новий_сніп, bz2.BZ2File(вікі_викид_боріг_путь, 'rb') as сніп:
    #         for дані in iter(lambda : сніп.read(100 * 1024), b''):
    #             новий_сніп.write(дані)
    ######

    ######
    # для gz
    вікі_викид_боріг_путь = tf.keras.utils.get_file(
        "вікі_викид.txt.tar.gz",
        ПОСИЛАННЯ_НА_ВИКИД,
        extract=True,
        cache_dir=ПУТЬ_ТИМЧ_СНІПНИЦІ,
        cache_subdir="вікі",
    )

    if not вікі_пис_путь.is_file():
        with gzip.open(вікі_викид_боріг_путь, "rb") as сніп_з:
            with open(вікі_пис_путь, "wb") as снів_в:
                shutil.copyfileobj(сніп_з, снів_в)
    #####

    #####
    # для xml

    # вікі_викид_хмл = вікі_викид_пис_путь.read_text()
    # вікі_викид_розбір = parseString(вікі_викид_хмл)

    # вікі_дані = []
    # for стор in вікі_викид_розбір.getElementsByTagName('page'):
    #     загол = стор.getElementsByTagName('title')[0].firstChild
    #     вміст = стор.getElementsByTagName('text')[0].firstChild
    #     if загол and вміст:
    #         вікі_дані.append(
    #             [
    #                 загол.nodeValue,
    #                 вміст.nodeValue
    #             ]
    #         )

    # # Write the `tf.train.Example` observations to the file.
    # with tf.io.TFRecordWriter(вікі_тф_путь.absolute().as_posix()) as писач:
    #     for загол, вміст in вікі_дані:
    #         своїсть = {
    #             'загол': tf.train.Feature(bytes_list=tf.train.BytesList(value=[загол.encode('utf-8')])),
    #             'вміст': tf.train.Feature(bytes_list=tf.train.BytesList(value=[вміст.encode('utf-8')])),

    #         }

    #         писач.write(
    #             tf.train.Example(features=tf.train.Features(feature=своїсть)).SerializeToString()
    #         )
    #####


def витяг_вікі_тф_хмл():
    опис_своїстей = {
        "загол": tf.io.FixedLenFeature([], tf.string, default_value=""),
        "вміст": tf.io.FixedLenFeature([], tf.string, default_value=""),
    }

    сира_нб = tf.data.TFRecordDataset([вікі_тф_путь])
    нб = сира_нб.map(lambda з: tf.io.parse_single_example(з, опис_своїстей))
    # нб = сира_нб.map(lambda з: tf.train.Example.FromString(з.numpy()))

    for з in нб:
        if "гривня" in з["вміст"].numpy().decode("utf-8"):
            print(з["загол"].numpy().decode("utf-8"))

    return нб


def витяг_берт_словник_по_вікі():
    нб = tf.data.TextLineDataset(вікі_пис_путь)
    # вличманювач = tf_text.UnicodeScriptTokenizer()
    # вличнанена_нб = нб.map(lambda x: вличманювач.tokenize(tf_text.case_fold_utf8(x)))

    # вличнанена_нб = вличнанена_нб.take(1000).cache().prefetch(tf.data.AUTOTUNE)

    # частотний_словник = collections.Counter()
    # for слова, кількості in [np.unique(ряд_слів, return_counts=True) for ряд_слів in вличнанена_нб.as_numpy_iterator()]:
    #     for вк, слово in enumerate(слова):
    #         частотний_словник[слово.decode('utf-8')] += кількості[вк]

    # слова, кількості = np.unique(chain(вличнанена_нб.as_numpy_iterator()), return_counts=True)

    # for вк, слово in enumerate(слова):
    #     частотний_словник[слово] = кількості[вк]

    # for toks in вличнанена_нб.ragged_batch(1000):
    #     toks = tf.reshape(toks, [-1])
    #     for tok in toks.numpy():
    #         частотний_словник[tok] += 1

    # vocab = [tok for tok, count in частотний_словник.most_common(VOCAB_SIZE)]

    bert_vocab_args = dict(
        # The target vocabulary size
        vocab_size=8000,
        # Reserved tokens that must be included in the vocabulary
        reserved_tokens=reserved_tokens,
        # Arguments for `text.BertTokenizer`
        bert_tokenizer_params=basic_tokenizer_params,
        # Arguments for `wordpiece_vocab.wordpiece_tokenizer_learner_lib.learn`
        learn_params=learn_params,
    )

    берт_словник = bert_vocab.bert_vocab_from_dataset(
        нб.take(20000),
        # нб.take(20000).batch(5000).prefetch(2),
        **bert_vocab_args,
    )

    with open(БЕРТ_СЛОВНИК_ПУТЬ, "w") as f:
        for личман in берт_словник:
            print(личман, file=f)

    return берт_словник


def витяг_берт_словник_по_частотних_та_товкних_даних():
    початк_сзд_пд = pd.read_csv(
        ПУТЬ_ДАНИХ_НАВЧАННЯ,
        # nrows=10e3
    )

    частотний_словник_пд = початк_сзд_пд[["слово", "повнаЧастота"]].sort_values(
        "повнаЧастота", ascending=False
    )


    bert_vocab_args = dict(
        # The target vocabulary size
        vocab_size=5000,
        # Reserved tokens that must be included in the vocabulary
        reserved_tokens=reserved_tokens,
        # Arguments for `text.BertTokenizer`
        bert_tokenizer_params=basic_tokenizer_params,
        # Arguments for `wordpiece_vocab.wordpiece_tokenizer_learner_lib.learn`
        learn_params=learn_params,
    )

    берт_словник_товкний = bert_vocab.bert_vocab_from_dataset(
        tf.data.Dataset.from_tensor_slices(початк_сзд_пд['товк'].fillna('').map(lambda x: x.replace(u"\u0301", "").lower())).take(20000),
        # нб.take(20000).batch(5000).prefetch(2),
        **bert_vocab_args,
    )

    берт_словник_частотний = wordpiece_tokenizer_learner_lib.learn(
        list(частотний_словник_пд[:100_000].itertuples(index=False, name=None)),
        10000,
        reserved_tokens,
        **learn_params,
    )

    with open(БЕРТ_СЛОВНИК_ПУТЬ, "w") as f:
        for личман in берт_словник_частотний:
            print(личман, file=f)
        for личман in берт_словник_товкний:
            if личман not in берт_словник_частотний:
                print(личман, file=f)


# витяг_берт_словник_по_частотних_та_товкних_даних()

# ручити_вікі_тф()
# витяг_вікі_тф()
# витяг_берт_словник_по_вікі()

True
