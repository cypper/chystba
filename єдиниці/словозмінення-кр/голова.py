import tensorflow as tf
from передпорання import в_передпорання_слів
from допоміжне import розділити_набірку
from бгал import створити_бгал
from загальне import СЛОВНИК_ЧМ, ПУТЬ_ТИМЧ_СНІПНИЦІ, НАЙБ_ЛИЧМАНІВ_ТОВКУ, НАЙБ_ЛИЧМАНІВ_СЛОВА
from дані import витяг_всіх_даних

print("-" * 100 + "\n" * 4)
print(tf.__version__)


# os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
print("GPUs: ", len(tf.config.list_physical_devices("GPU")))

РОЗМІР_ПУЧКА = 64
ВІДН_РОЗМІР_ВИВІРУ = 0.2
ВІДН_РОЗМІР_СПИТУ = 0.2

словник_щурба = СЛОВНИК_ЧМ
назва_щурба = "частинаМови"

сзд_пд = витяг_всіх_даних()

(передпорання_слів, довж_словника_слів, _) = в_передпорання_слів(НАЙБ_ЛИЧМАНІВ_СЛОВА)
сзд_слова = передпорання_слів(tf.convert_to_tensor(сзд_пд["слово"]))
print("сзд_слова")

(передпорання_товку, довж_словника_товку, _) = в_передпорання_слів(НАЙБ_ЛИЧМАНІВ_ТОВКУ+1)
передпораний_товк = передпорання_товку(tf.convert_to_tensor(сзд_пд["товк"]))

сзд_товку = передпораний_товк[:, :-1]
print("сзд_товку")

сзд_товку_зсунуте = передпораний_товк[:, 1:]
print("сзд_товку_зсунуте")


# (передпорання_луки, довж_словника_луки) = в_передпорання_щурбу(
#     tf.convert_to_tensor(словник_щурба)
# )
# сзд_лука = передпорання_луки(tf.ragged.constant(list(сзд_пд[назва_щурба])))
# print("сзд_лука")

нб = tf.data.Dataset.from_tensor_slices(((сзд_слова, сзд_товку), сзд_товку_зсунуте))

прохід = iter(нб)
for вк in range(10):
    print(сзд_пд["слово"][вк], сзд_пд["товк"][вк], next(прохід))

нб = нб.shuffle(20000)

човп_нб, вивір_нб, спит_нб = розділити_набірку(нб, сзд_слова.shape[0], ВІДН_РОЗМІР_ВИВІРУ, ВІДН_РОЗМІР_СПИТУ)

човп_нб = човп_нб.shuffle(20000).batch(РОЗМІР_ПУЧКА).prefetch(buffer_size=tf.data.AUTOTUNE).repeat()
вивір_нб = вивір_нб.shuffle(20000).batch(РОЗМІР_ПУЧКА).prefetch(buffer_size=tf.data.AUTOTUNE).repeat()
спит_нб = спит_нб.shuffle(20000).batch(РОЗМІР_ПУЧКА).prefetch(buffer_size=tf.data.AUTOTUNE)

бгал = створити_бгал(довж_словника_слів, довж_словника_товку)


tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir="./тимч/пірописи", embeddings_freq=1, histogram_freq=1)
бгал.fit(
    човп_нб,
    validation_data=вивір_нб,

    epochs=50,
    steps_per_epoch=5e2,
    validation_steps=1e2,

    # epochs=1,
    # steps_per_epoch=1,
    # validation_steps=1,

    callbacks=[tensorboard_callback],
)

бгал.summary()

# tf.saved_model.save(бгал, ПУТЬ_ТИМЧ_СНІПНИЦІ/'бгал')
бгал.save(ПУТЬ_ТИМЧ_СНІПНИЦІ / "бгал")
бгал.save(ПУТЬ_ТИМЧ_СНІПНИЦІ / "бгал.keras")
бгал.save_weights(ПУТЬ_ТИМЧ_СНІПНИЦІ / 'бгал-тяжі/тяжі')

# мірила = бгал.evaluate(
#     спит_нб, return_dict=True
# )

# print(мірила)

# TEMP_FOLDER_PATH='/home/cypper/Документи/чистьба/служник/єдиниці/словозмінення-кр/тимч' python голова.py
True
