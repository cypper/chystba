def розділити_на_частини(панда, відн_розмір_част1):
    розмір_част1 = int(відн_розмір_част1 * панда.shape[0])

    return (панда[:розмір_част1], панда[розмір_част1:])

def розділити_набірку(нб, розмір_нб, відн_розмір_вивіру, відн_розмір_спиту):
    train_size = int((1 - відн_розмір_спиту - відн_розмір_вивіру) * розмір_нб)
    val_size = int(відн_розмір_вивіру * розмір_нб)
    test_size = int(відн_розмір_спиту * розмір_нб)

    train_dataset = нб.take(train_size)
    решта_нб = нб.skip(train_size)
    val_dataset = решта_нб.skip(val_size)
    test_dataset = решта_нб.take(test_size)

    return (train_dataset, val_dataset, test_dataset)