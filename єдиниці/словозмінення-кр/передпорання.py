import tensorflow as tf
from tensorflow.keras import layers

import tensorflow_text as tf_text
from загальне import БЕРТ_СЛОВНИК_ПУТЬ, НЕВІДОВИЙ_ЛИЧМАН, ВК_ПОЧ_ЛИЧМАН, ВК_КІН_ЛИЧМАН
from берт_словник import basic_tokenizer_params


@tf.function
def propysky(vyslid, довж):
    if isinstance(vyslid, tf.RaggedTensor):
        # вислід = vyslid.to_tensor()

        найбдовж = tf.reduce_max(vyslid.row_lengths())
        rows_to_pad = tf.add(
            tf.abs(vyslid.row_lengths() - найбдовж),
            tf.reduce_max([довж - найбдовж, 0]),
        )
        zeros_to_pad = tf.zeros(tf.reduce_sum(rows_to_pad), dtype=vyslid.dtype)
        вислід = tf.concat(
            [
                # пропуски зліва
                vyslid,
                tf.RaggedTensor.from_row_lengths(
                    values=zeros_to_pad, row_lengths=rows_to_pad
                ),
                # пропуски зправа
                # tf.RaggedTensor.from_row_lengths(
                #     values=zeros_to_pad, row_lengths=rows_to_pad
                # ),
                # vyslid,
            ],
            axis=-1,
        ).to_tensor()
    else:
        вислід = vyslid

    твар = tf.shape(вислід)
    # обрізаєм зправа
    вислід = tf.slice(
        вислід,
        tf.zeros(tf.rank(твар) + 1, dtype=tf.int32),
        tf.concat([твар[0:-1], tf.TensorShape(довж)], axis=0),
    )

    return вислід


class VlychmanyuvachPoBykvi(tf.keras.layers.Layer):
    def __init__(я, довж=20):
        super().__init__()
        я.вличманювач = tf_text.UnicodeCharTokenizer()
        я.довж = довж

    def call(я, pys):
        вислід = я.вличманювач.tokenize(pys)
        вислід = propysky(вислід, я.довж)

        return вислід


class VlychmanyuvachBert(tf.keras.layers.Layer):
    def __init__(я, довж=10):
        super().__init__()
        # я.вличманювач = tf_text.UnicodeCharTokenizer()
        я.вличманювач = витяг_берт_вличманювач()
        я.довж = довж

    def call(я, pys):
        вислід = я.вличманювач.tokenize(pys)
        вислід = вислід.merge_dims(-2, -1)
        твар_вставки = tf.concat([вислід.shape[0:-1], tf.TensorShape(1)], axis=0)
        вислід = tf.concat([tf.cast(tf.fill(твар_вставки, ВК_ПОЧ_ЛИЧМАН), dtype=tf.int64), вислід, tf.cast(tf.fill(твар_вставки, ВК_КІН_ЛИЧМАН), dtype=tf.int64)], -1)
        вислід = propysky(вислід, я.довж)

        return вислід

    def detokenize(я, вкази):
        return я.вличманювач.detokenize(вкази)


def витяг_берт_вличманювач():
    вличм = tf_text.BertTokenizer(
        БЕРТ_СЛОВНИК_ПУТЬ.as_posix(),
        unknown_token=НЕВІДОВИЙ_ЛИЧМАН,
        **basic_tokenizer_params,
    )
    return вличм


def витяг_берт_словник():
    return list(tf.data.TextLineDataset(БЕРТ_СЛОВНИК_ПУТЬ).as_numpy_iterator())


def витяг_буквовий_словник():
    return tf.convert_to_tensor(
        tf_text.UnicodeCharTokenizer()
        .tokenize(
            list("абвгґдеєжзиіїйклмнопрстуфхцчшщьюяАБВГҐДЕЄЖЗИІЇЙКЛМНОПРСТУФХЦЧШЩЬЮЯ-'")
        )
        .numpy()
        .flat
    )
    # return tf_text.UnicodeCharTokenizer().tokenize(list('абвгґдеєжзиіїйклмнопрстуфхцчшщьюяАБВГҐДЕЄЖЗИІЇЙКЛМНОПРСТУФХЦЧШЩЬЮЯ-\''))


def в_передпорання_слів(довж):
    вличманювач = VlychmanyuvachBert(довж)

    словник = витяг_берт_словник()

    # словник = витяг_буквовий_словник()
    # словник_щільник = tf.keras.layers.IntegerLookup(
    #     vocabulary=словник, output_mode="int"
    # )

    # передпорання = tf.keras.Sequential(
    #     [
    #         вличманювач,
    #         # словник_щільник
    #     ]
    # )

    return (вличманювач, len(словник), словник)


def в_передпорання_щурбу(словник):
    ськач = layers.StringLookup(output_mode="count", vocabulary=словник)

    передпорання = tf.keras.Sequential(
        [
            ськач,
            tf.keras.layers.Lambda(
                lambda x: x
                / tf.reshape(
                    tf.repeat(
                        tf.math.reduce_sum(x, axis=1), repeats=x.shape[1], axis=0
                    ),
                    x.shape,
                )
            ),
        ]
    )

    return (передпорання, len(ськач.get_vocabulary()))
