import pathlib
import os
import tensorflow as tf
import numpy as np
from загальне import СЛОВНИК_ЧМ, ВК_ПОЧ_ЛИЧМАН, ВК_КІН_ЛИЧМАН, НАЙБ_ЛИЧМАНІВ_ТОВКУ, НАЙБ_ЛИЧМАНІВ_СЛОВА
from дані import витяг_всіх_даних
from передпорання import в_передпорання_щурбу, в_передпорання_слів
from бгал import створити_бгал

ПУТЬ_ТИМЧ_СНІПНИЦІ = pathlib.Path(os.environ["TEMP_FOLDER_PATH"])


class TFLiteModel:
    @classmethod
    def from_file(cls, model_path):
        return TFLiteModel(tf.lite.Interpreter(model_path=model_path))

    @classmethod
    def from_keras_model(cls, kmodel):
        converter = tf.lite.TFLiteConverter.from_keras_model(kmodel)
        tflite_model = converter.convert()

        interp = tf.lite.Interpreter(model_content=tflite_model)

        return TFLiteModel(interp)

    @classmethod
    def from_saved_model(cls, dir):
        converter = tf.lite.TFLiteConverter.from_saved_model(str(dir))
        converter.target_spec.supported_ops = [
            tf.lite.OpsSet.TFLITE_BUILTINS,  # enable LiteRT ops.
            tf.lite.OpsSet.SELECT_TF_OPS,  # enable TensorFlow ops.
        ]
        tflite_model = converter.convert()

        interp = tf.lite.Interpreter(model_content=tflite_model)

        return TFLiteModel(interp)

    def __init__(self, interpreter):
        self.interpreter = interpreter
        self.interpreter.allocate_tensors()
        input_det = self.interpreter.get_input_details()[0]
        output_det = self.interpreter.get_output_details()[0]
        self.input_index = input_det["index"]
        self.output_index = output_det["index"]
        self.input_shape = input_det["shape"]
        self.output_shape = output_det["shape"]
        self.input_dtype = input_det["dtype"]
        self.output_dtype = output_det["dtype"]

    def __call__(я, inp):
        return я.predict(inp)

    def predict(self, inp):
        inp = inp.numpy().astype(self.input_dtype)
        count = inp.shape[0]
        out = np.zeros((count, self.output_shape[1]), dtype=self.output_dtype)
        for i in range(count):
            self.interpreter.set_tensor(self.input_index, inp[i : i + 1])
            self.interpreter.invoke()
            out[i] = self.interpreter.get_tensor(self.output_index)[0]
        return out

    def predict_single(self, inp):
        """Like predict(), but only for a single record. The input data can be a Python list."""
        inp = np.array([inp], dtype=self.input_dtype)
        self.interpreter.set_tensor(self.input_index, inp)
        self.interpreter.invoke()
        out = self.interpreter.get_tensor(self.output_index)
        return out[0]

class Товкник(tf.Module):
  def __init__(self, transformer):
    self.transformer = transformer

  def __call__(self, вхід, max_length):
    # As the output language is English, initialize the output with the
    # English `[START]` token.
    start = ВК_ПОЧ_ЛИЧМАН
    end = ВК_КІН_ЛИЧМАН

    # `tf.TensorArray` is required here (instead of a Python list), so that the
    # dynamic-loop can be traced by `tf.function`.
    output_array = tf.TensorArray(dtype=tf.int64, size=0, dynamic_size=True)
    output_array = output_array.write(0, start)

    for i in tf.range(max_length):
      output = tf.transpose(output_array.stack())
      predictions = self.transformer([tf.convert_to_tensor([вхід]), tf.convert_to_tensor([output])], training=False)

      # Select the last token from the `seq_len` dimension.
      predictions = predictions[0, -1:, :]  # Shape `(batch_size, 1, vocab_size)`.

      predicted_id = tf.argmax(predictions, axis=-1)

      # Concatenate the `predicted_id` to the output which is given to the
      # decoder as its input.
      output_array = output_array.write(i+1, predicted_id[0])

      if predicted_id == end:
        break

    output = tf.transpose(output_array.stack())
    # # The output shape is `(1, tokens)`.
    # text = tokenizers.en.detokenize(output)[0]  # Shape: `()`.

    return output

def ввіз_бгалу():
    (_, довж_словника_слів, _) = в_передпорання_слів(НАЙБ_ЛИЧМАНІВ_СЛОВА)
    (_, довж_словника_товку, _) = в_передпорання_слів(НАЙБ_ЛИЧМАНІВ_ТОВКУ+1)
    бгал = створити_бгал(довж_словника_слів, довж_словника_товку)
    бгал.load_weights(ПУТЬ_ТИМЧ_СНІПНИЦІ / 'бгал-тяжі/тяжі')   

    # бгал = tf.saved_model.load(ПУТЬ_ТИМЧ_СНІПНИЦІ/'бгал')
    # бгал = tf.keras.models.load_model(ПУТЬ_ТИМЧ_СНІПНИЦІ / "бгал")
    # бгал = TFLiteModel.from_keras_model(tf.keras.models.load_model(ПУТЬ_ТИМЧ_СНІПНИЦІ/'бгал'))
    # бгал = TFLiteModel.from_saved_model(ПУТЬ_ТИМЧ_СНІПНИЦІ/'бгал')

    return бгал


def ручний_спит():


    бгал = ввіз_бгалу()

    приклади = [
            "вийти",
            "красиво",
            "весна",
            "дорога",
            "війна",
            "розійдений",
            "гарною",
            "навчанням",
            "восьмий",
            "або",
            "що",
            "ау",
            "сморід",
            "лівнути",
            "їбати",
            "хуй"
        ]
    приклади_тф = tf.constant(приклади)

    (передпорання_слів, довж_словника_слів, словник) = в_передпорання_слів(НАЙБ_ЛИЧМАНІВ_СЛОВА)
    сзд_слова = передпорання_слів(приклади_тф)

    for вк in range(len(приклади)):
        товкник = Товкник(бгал)
        товк = товкник(сзд_слова[вк], НАЙБ_ЛИЧМАНІВ_ТОВКУ)
        товк = передпорання_слів.detokenize([товк])
        товк = b' '.join(товк.to_list()[0]).decode()
        print(приклади[вк], товк)
    # нб = tf.data.Dataset.from_tensor_slices((сзд_слова[0], tf.convert_to_tensor([ВК_ПОЧ_ЛИЧМАН])))
    # ймовірності = tf.math.softmax(бгал([tf.convert_to_tensor([сзд_слова[0]]), tf.convert_to_tensor([вихід])], training=False)).numpy()
    # ймовірності = tf.math.softmax(бгал((сзд_слова, tf.fill(сзд_слова.shape[0], ВК_ПОЧ_ЛИЧМАН)), training=False)).numpy()

    # округлені_ймовірності = np.round(ймовірності, 2)

    # for вк in range(len(приклади)):
    #     print(приклади[вк].numpy().decode(), віща_слово[вк], округлені_ймовірності[вк])


def самочинний_спит():
    словник_щурба = СЛОВНИК_ЧМ
    назва_щурба = "частинаМови"

    сзд_пд = витяг_всіх_даних(назва_щурба, словник_щурба, кількість_на_щурб=1000)
    сзд_пд = сзд_пд[сзд_пд[назва_щурба].map(lambda x: len(x)) == 1]

    сзд_своїсті_пд = tf.convert_to_tensor(сзд_пд["слово"])

    (передпорання_луки, _) = в_передпорання_щурбу(словник_щурба)
    сзд_лука_пд = передпорання_луки(tf.ragged.constant(list(сзд_пд[назва_щурба])))

    бгал = ввіз_бгалу()

    віща = бгал(сзд_своїсті_пд).numpy()

    правд_щурбів = tf.math.argmax(сзд_лука_пд, axis=-1).numpy()

    for вк, знач in enumerate(словник_щурба):
        вк_щурба = вк + 1
        (вкази_правд_щурба,) = np.where(правд_щурбів == вк_щурба)

        ксть = вкази_правд_щурба.shape[0]
        if ксть == 0:
            continue

        правильність = правд_щурбів[вкази_правд_щурба] == np.argmax(віща[вкази_правд_щурба], axis=-1)
        хибні_віщі = np.round(tf.math.softmax(віща[вкази_правд_щурба][правильність == False]).numpy(), 2)
        хибні_двійк_слова = сзд_своїсті_пд.numpy()[вкази_правд_щурба][правильність == False]
        хибні_слова = [двійк_слово.decode() for двійк_слово in хибні_двійк_слова]

        ксть_правильних = (
            правильність
            .sum()
        )

        част_правильних = ксть_правильних / ксть
        print(знач, част_правильних * 100, "%")

def спит(приклади):
    ймовірності = tf.math.softmax(бгал(передпорання_слів(приклади))).numpy()

    віща_щурб = [СЛОВНИК_ЧМ[найбвк - 1] for найбвк in np.argmax(ймовірності, -1)]
    округлені_ймовірності = np.round(ймовірності, 2)

    for вк in range(len(приклади)):
        print(приклади[вк].numpy().decode(), віща_щурб[вк], округлені_ймовірності[вк])

# самочинний_спит()
ручний_спит()

True
