import { перекладеніПоля } from './переклад.js';

interface ПокращенаОбмета extends NonNullable<unknown> {
  hasOwnProperty<K extends PropertyKey>(key: K): this is Record<K, unknown>;
}
export type НевідомаОбмета = ПокращенаОбмета;

type ПерекладеніПоля = typeof перекладеніПоля;

type ВидалитиПоляНіколи<T> = {
  [K in keyof T as T[K] extends never ? never : K]: T[K];
};

// https://github.com/Microsoft/TypeScript/issues/27024#issuecomment-509504856
type ЧиОднакові<X, Y, A, B> =
  (<T>() => T extends X ? 1 : 2) extends <T>() => T extends Y ? 1 : 2 ? A : B;

// type ЧиОднакові<X, Y, A, B> =
//   [2] & [0, 1, X] extends [2] & [0, 1, Y] & [0, infer W, unknown]
//   ? W extends 1 ? B : A
//   : B;

type ПисальніКлючі<T> = {
  [P in keyof T]: ЧиОднакові<
    { [Q in P]: T[P] },
    { -readonly [Q in P]: T[P] },
    P,
    never
  >;
}[keyof T];
// type ТількиЧитальніКлючі<T> = Exclude<keyof T, ПисальніКлючі<T>>;

type ЧиПисальнийКлюч<T, Key extends keyof T, A, B> =
  Key extends ПисальніКлючі<T> ? A : B;

export type ОчищеніПоляОбмета<T extends НевідомаОбмета> = ВидалитиПоляНіколи<{
  readonly [Key in keyof ПерекладеніПоля as ПерекладеніПоля[Key]]: T extends {
    [_ in Key]: infer B;
  }
    ? ЧиПисальнийКлюч<T, Key, never, B>
    : never;
}> &
  ВидалитиПоляНіколи<{
    -readonly [Key in keyof ПерекладеніПоля as ПерекладеніПоля[Key]]: T extends {
      [_ in Key]: infer B;
    }
      ? ЧиПисальнийКлюч<T, Key, B, never>
      : never;
  }> &
  Partial<
    ВидалитиПоляНіколи<{
      readonly [Key in keyof ПерекладеніПоля as ПерекладеніПоля[Key]]: T extends {
        [_ in Key]?: infer B;
      }
        ? ЧиПисальнийКлюч<T, Key, never, B>
        : never;
    }> &
      ВидалитиПоляНіколи<{
        -readonly [Key in keyof ПерекладеніПоля as ПерекладеніПоля[Key]]: T extends {
          [_ in Key]?: infer B;
        }
          ? ЧиПисальнийКлюч<T, Key, B, never>
          : never;
      }>
  >;

export type ОчищеннаОбмета<T extends НевідомаОбмета> = T & ОчищеніПоляОбмета<T>;

// interface CopyOfConcatArray<T> {
//   readonly length: number;
//   readonly [n: number]: T;
//   join(separator?: string): string;
//   slice(start?: number, end?: number): CopyOfAnArray<T>;
// }
// export interface CopyOfAnArray<T> {
//   /**
//    * Gets or sets the length of the array. This is a number one higher than the highest index in the array.
//    */
//   length: number;
//   /**
//    * Returns a string representation of an array.
//    */
//   toString(): string;
//   /**
//    * Returns a string representation of an array. The elements are converted to string using their toLocaleString methods.
//    */
//   toLocaleString(): string;
//   /**
//    * Removes the last element from an array and returns it.
//    * If the array is empty, undefined is returned and the array is not modified.
//    */
//   pop(): T | undefined;
//   /**
//    * Appends new elements to the end of an array, and returns the new length of the array.
//    * @param items New elements to add to the array.
//    */
//   push(...items: T[]): number;
//   /**
//    * Combines two or more arrays.
//    * This method returns a new array without modifying any existing arrays.
//    * @param items Additional arrays and/or items to add to the end of the array.
//    */
//   concat(...items: CopyOfConcatArray<T>[]): T[];
//   /**
//    * Combines two or more arrays.
//    * This method returns a new array without modifying any existing arrays.
//    * @param items Additional arrays and/or items to add to the end of the array.
//    */
//   concat(...items: (T | CopyOfConcatArray<T>)[]): T[];
//   /**
//    * Adds all the elements of an array into a string, separated by the specified separator string.
//    * @param separator A string used to separate one element of the array from the next in the resulting string. If omitted, the array elements are separated with a comma.
//    */
//   join(separator?: string): string;
//   /**
//    * Reverses the elements in an array in place.
//    * This method mutates the array and returns a reference to the same array.
//    */
//   reverse(): T[];
//   /**
//    * Removes the first element from an array and returns it.
//    * If the array is empty, undefined is returned and the array is not modified.
//    */
//   shift(): T | undefined;
//   /**
//    * Returns a copy of a section of an array.
//    * For both start and end, a negative index can be used to indicate an offset from the end of the array.
//    * For example, -2 refers to the second to last element of the array.
//    * @param start The beginning index of the specified portion of the array.
//    * If start is undefined, then the slice begins at index 0.
//    * @param end The end index of the specified portion of the array. This is exclusive of the element at the index 'end'.
//    * If end is undefined, then the slice extends to the end of the array.
//    */
//   slice(start?: number, end?: number): T[];
//   /**
//    * Sorts an array in place.
//    * This method mutates the array and returns a reference to the same array.
//    * @param compareFn Function used to determine the order of the elements. It is expected to return
//    * a negative value if the first argument is less than the second argument, zero if they're equal, and a positive
//    * value otherwise. If omitted, the elements are sorted in ascending, ASCII character order.
//    * ```ts
//    * [11,2,22,1].sort((a, b) => a - b)
//    * ```
//    */
//   sort(compareFn?: (a: T, b: T) => number): this;
//   /**
//    * Removes elements from an array and, if necessary, inserts new elements in their place, returning the deleted elements.
//    * @param start The zero-based location in the array from which to start removing elements.
//    * @param deleteCount The number of elements to remove.
//    * @returns An array containing the elements that were deleted.
//    */
//   splice(start: number, deleteCount?: number): T[];
//   /**
//    * Removes elements from an array and, if necessary, inserts new elements in their place, returning the deleted elements.
//    * @param start The zero-based location in the array from which to start removing elements.
//    * @param deleteCount The number of elements to remove.
//    * @param items Elements to insert into the array in place of the deleted elements.
//    * @returns An array containing the elements that were deleted.
//    */
//   splice(start: number, deleteCount: number, ...items: T[]): T[];
//   /**
//    * Inserts new elements at the start of an array, and returns the new length of the array.
//    * @param items Elements to insert at the start of the array.
//    */
//   unshift(...items: T[]): number;
//   /**
//    * Returns the index of the first occurrence of a value in an array, or -1 if it is not present.
//    * @param searchElement The value to locate in the array.
//    * @param fromIndex The array index at which to begin the search. If fromIndex is omitted, the search starts at index 0.
//    */
//   indexOf(searchElement: T, fromIndex?: number): number;
//   /**
//    * Returns the index of the last occurrence of a specified value in an array, or -1 if it is not present.
//    * @param searchElement The value to locate in the array.
//    * @param fromIndex The array index at which to begin searching backward. If fromIndex is omitted, the search starts at the last index in the array.
//    */
//   lastIndexOf(searchElement: T, fromIndex?: number): number;
//   /**
//    * Determines whether all the members of an array satisfy the specified test.
//    * @param predicate A function that accepts up to three arguments. The every method calls
//    * the predicate function for each element in the array until the predicate returns a value
//    * which is coercible to the Boolean value false, or until the end of the array.
//    * @param thisArg An object to which the this keyword can refer in the predicate function.
//    * If thisArg is omitted, undefined is used as the this value.
//    */
//   every<S extends T>(predicate: (value: T, index: number, array: T[]) => value is S, thisArg?: any): this is S[];
//   /**
//    * Determines whether all the members of an array satisfy the specified test.
//    * @param predicate A function that accepts up to three arguments. The every method calls
//    * the predicate function for each element in the array until the predicate returns a value
//    * which is coercible to the Boolean value false, or until the end of the array.
//    * @param thisArg An object to which the this keyword can refer in the predicate function.
//    * If thisArg is omitted, undefined is used as the this value.
//    */
//   every(predicate: (value: T, index: number, array: T[]) => unknown, thisArg?: any): boolean;
//   /**
//    * Determines whether the specified callback function returns true for any element of an array.
//    * @param predicate A function that accepts up to three arguments. The some method calls
//    * the predicate function for each element in the array until the predicate returns a value
//    * which is coercible to the Boolean value true, or until the end of the array.
//    * @param thisArg An object to which the this keyword can refer in the predicate function.
//    * If thisArg is omitted, undefined is used as the this value.
//    */
//   some(predicate: (value: T, index: number, array: T[]) => unknown, thisArg?: any): boolean;
//   /**
//    * Performs the specified action for each element in an array.
//    * @param callbackfn  A function that accepts up to three arguments. forEach calls the callbackfn function one time for each element in the array.
//    * @param thisArg  An object to which the this keyword can refer in the callbackfn function. If thisArg is omitted, undefined is used as the this value.
//    */
//   forEach(callbackfn: (value: T, index: number, array: T[]) => void, thisArg?: any): void;
//   /**
//    * Calls a defined callback function on each element of an array, and returns an array that contains the results.
//    * @param callbackfn A function that accepts up to three arguments. The map method calls the callbackfn function one time for each element in the array.
//    * @param thisArg An object to which the this keyword can refer in the callbackfn function. If thisArg is omitted, undefined is used as the this value.
//    */
//   map<U>(callbackfn: (value: T, index: number, array: T[]) => U, thisArg?: any): U[];
//   /**
//    * Returns the elements of an array that meet the condition specified in a callback function.
//    * @param predicate A function that accepts up to three arguments. The filter method calls the predicate function one time for each element in the array.
//    * @param thisArg An object to which the this keyword can refer in the predicate function. If thisArg is omitted, undefined is used as the this value.
//    */
//   filter<S extends T>(predicate: (value: T, index: number, array: T[]) => value is S, thisArg?: any): S[];
//   /**
//    * Returns the elements of an array that meet the condition specified in a callback function.
//    * @param predicate A function that accepts up to three arguments. The filter method calls the predicate function one time for each element in the array.
//    * @param thisArg An object to which the this keyword can refer in the predicate function. If thisArg is omitted, undefined is used as the this value.
//    */
//   filter(predicate: (value: T, index: number, array: T[]) => unknown, thisArg?: any): T[];
//   /**
//    * Calls the specified callback function for all the elements in an array. The return value of the callback function is the accumulated result, and is provided as an argument in the next call to the callback function.
//    * @param callbackfn A function that accepts up to four arguments. The reduce method calls the callbackfn function one time for each element in the array.
//    * @param initialValue If initialValue is specified, it is used as the initial value to start the accumulation. The first call to the callbackfn function provides this value as an argument instead of an array value.
//    */
//   reduce(callbackfn: (previousValue: T, currentValue: T, currentIndex: number, array: T[]) => T): T;
//   reduce(callbackfn: (previousValue: T, currentValue: T, currentIndex: number, array: T[]) => T, initialValue: T): T;
//   /**
//    * Calls the specified callback function for all the elements in an array. The return value of the callback function is the accumulated result, and is provided as an argument in the next call to the callback function.
//    * @param callbackfn A function that accepts up to four arguments. The reduce method calls the callbackfn function one time for each element in the array.
//    * @param initialValue If initialValue is specified, it is used as the initial value to start the accumulation. The first call to the callbackfn function provides this value as an argument instead of an array value.
//    */
//   reduce<U>(callbackfn: (previousValue: U, currentValue: T, currentIndex: number, array: T[]) => U, initialValue: U): U;
//   /**
//    * Calls the specified callback function for all the elements in an array, in descending order. The return value of the callback function is the accumulated result, and is provided as an argument in the next call to the callback function.
//    * @param callbackfn A function that accepts up to four arguments. The reduceRight method calls the callbackfn function one time for each element in the array.
//    * @param initialValue If initialValue is specified, it is used as the initial value to start the accumulation. The first call to the callbackfn function provides this value as an argument instead of an array value.
//    */
//   reduceRight(callbackfn: (previousValue: T, currentValue: T, currentIndex: number, array: T[]) => T): T;
//   reduceRight(callbackfn: (previousValue: T, currentValue: T, currentIndex: number, array: T[]) => T, initialValue: T): T;
//   /**
//    * Calls the specified callback function for all the elements in an array, in descending order. The return value of the callback function is the accumulated result, and is provided as an argument in the next call to the callback function.
//    * @param callbackfn A function that accepts up to four arguments. The reduceRight method calls the callbackfn function one time for each element in the array.
//    * @param initialValue If initialValue is specified, it is used as the initial value to start the accumulation. The first call to the callbackfn function provides this value as an argument instead of an array value.
//    */
//   reduceRight<U>(callbackfn: (previousValue: U, currentValue: T, currentIndex: number, array: T[]) => U, initialValue: U): U;

//   [n: number]: T;
// }
// export interface CopyOfAnArray<T> extends RelativeIndexable<T> {}
// export interface CopyOfAnArray<T> {
//   /**
//    * Returns the value of the first element in the array where predicate is true, and undefined
//    * otherwise.
//    * @param predicate find calls predicate once for each element of the array, in ascending
//    * order, until it finds one where predicate returns true. If such an element is found, find
//    * immediately returns that element value. Otherwise, find returns undefined.
//    * @param thisArg If provided, it will be used as the this value for each invocation of
//    * predicate. If it is not provided, undefined is used instead.
//    */
//   find<S extends T>(predicate: (value: T, index: number, obj: T[]) => value is S, thisArg?: any): S | undefined;
//   find(predicate: (value: T, index: number, obj: T[]) => unknown, thisArg?: any): T | undefined;

//   /**
//    * Returns the index of the first element in the array where predicate is true, and -1
//    * otherwise.
//    * @param predicate find calls predicate once for each element of the array, in ascending
//    * order, until it finds one where predicate returns true. If such an element is found,
//    * findIndex immediately returns that element index. Otherwise, findIndex returns -1.
//    * @param thisArg If provided, it will be used as the this value for each invocation of
//    * predicate. If it is not provided, undefined is used instead.
//    */
//   findIndex(predicate: (value: T, index: number, obj: T[]) => unknown, thisArg?: any): number;

//   /**
//    * Changes all array elements from `start` to `end` index to a static `value` and returns the modified array
//    * @param value value to fill array section with
//    * @param start index to start filling the array at. If start is negative, it is treated as
//    * length+start where length is the length of the array.
//    * @param end index to stop filling the array at. If end is negative, it is treated as
//    * length+end.
//    */
//   fill(value: T, start?: number, end?: number): this;

//   /**
//    * Returns the this object after copying a section of the array identified by start and end
//    * to the same array starting at position target
//    * @param target If target is negative, it is treated as length+target where length is the
//    * length of the array.
//    * @param start If start is negative, it is treated as length+start. If end is negative, it
//    * is treated as length+end.
//    * @param end If not specified, length of the this object is used as its default value.
//    */
//   copyWithin(target: number, start: number, end?: number): this;
// }
// interface CopyOfIterator<T, TReturn = any, TNext = undefined> {
//   // NOTE: 'next' is defined using a tuple to ensure we report the correct assignability errors in all places.
//   // @ts-ignore
//   next(...args: CopyOfAnArray<never> | CopyOfAnArray<TNext>): IteratorResult<T, TReturn>;
//   return?(value?: TReturn): IteratorResult<T, TReturn>;
//   throw?(e?: any): IteratorResult<T, TReturn>;
// }
// interface CopyOfIterableIterator<T> extends CopyOfIterator<T> {
//   [Symbol.iterator](): CopyOfIterableIterator<T>;
// }
// export interface CopyOfAnArray<T> {
//   /** Iterator */
//   [Symbol.iterator](): CopyOfIterableIterator<T>;

//   /**
//    * Returns an iterable of key, value pairs for every entry in the array
//    */
//   entries(): CopyOfIterableIterator<[number, T]>;

//   /**
//    * Returns an iterable of keys in the array
//    */
//   keys(): CopyOfIterableIterator<number>;

//   /**
//    * Returns an iterable of values in the array
//    */
//   values(): CopyOfIterableIterator<T>;
// }
// export interface CopyOfAnArray<T> {
//   /**
//    * Is an object whose properties have the value 'true'
//    * when they will be absent when used in a 'with' statement.
//    */
//   readonly [Symbol.unscopables]: {
//       [K in keyof any[]]?: boolean;
//   };
// }
// export interface CopyOfAnArray<T> {
//   /**
//    * Determines whether an array includes a certain element, returning true or false as appropriate.
//    * @param searchElement The element to search for.
//    * @param fromIndex The position in this array at which to begin searching for searchElement.
//    */
//   includes(searchElement: T, fromIndex?: number): boolean;
// }
// type FlatArray<Arr, Depth extends number> = {
//   "done": Arr,
//   "recur": Arr extends ReadonlyArray<infer InnerArr>
//       ? FlatArray<InnerArr, [-1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20][Depth]>
//       : Arr
// }[Depth extends -1 ? "done" : "recur"];
// export interface CopyOfAnArray<T> extends Pick<Array<T>, 'flatMap'> {

// /**
//  * Calls a defined callback function on each element of an array. Then, flattens the result into
//  * a new array.
//  * This is identical to a map followed by flat with depth 1.
//  *
//  * @param callback A function that accepts up to three arguments. The flatMap method calls the
//  * callback function one time for each element in the array.
//  * @param thisArg An object to which the this keyword can refer in the callback function. If
//  * thisArg is omitted, undefined is used as the this value.
//  */
// flatMap<U, This = undefined> (
//     callback: (this: This, value: T, index: number, array: T[]) => U | ReadonlyArray<U>,
//     thisArg?: This
// ): U[]

//   /**
//    * Returns a new array with all sub-array elements concatenated into it recursively up to the
//    * specified depth.
//    *
//    * @param depth The maximum recursion depth
//    */
//   flat<A, D extends number = 1>(
//       this: A,
//       depth?: D
//   ): FlatArray<A, D>[]
// }
