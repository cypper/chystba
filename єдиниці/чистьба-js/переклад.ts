export const перекладеніПоля = {
  map: 'по',
  length: 'довжина',
  split: 'розділити',
  slice: 'нарізати',

  flat: 'сплюснути',
  flatMap: 'сплюснутиПо',

  mapMap: 'поMap',
  lengthMap: 'довжинаMap',
  splitMap: 'розділитиMap',
  sliceMap: 'нарізатиMap',
} as const;
