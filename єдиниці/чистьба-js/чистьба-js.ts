import { НевідомаОбмета, ОчищеннаОбмета } from './види.js';
import { перекладеніПоля } from './переклад.js';

// export function зісколитиТаРозширитиОчищенимиПолями<T extends НевідомаОбмета>(
//   обмета: T,
// ): ОчищеннаОбмета<T> {
//   return розширитиОчищенимиПолями(Object.assign({}, обмета));
// }

export function розширитиОчищенимиПолями<T extends НевідомаОбмета>(
  обмета: T,
): ОчищеннаОбмета<T> {
  for (const [первинна, переклад] of Object.entries(перекладеніПоля)) {
    if (обмета.hasOwnProperty(первинна)) {
      const описник = Object.getOwnPropertyDescriptor(обмета, первинна);
      if (!описник) continue;

      if (['function', 'object'].includes(typeof обмета[первинна])) {
        Object.defineProperty(обмета, переклад, описник);
      }

      if (
        [
          'boolean',
          'string',
          'number',
          'symbol',
          'undefined',
          'bigint',
        ].includes(typeof обмета[первинна])
      ) {
        const отримувач = описник.hasOwnProperty('get')
          ? описник.get
          : function (this: typeof обмета) {
              return Reflect.get(this, первинна);
            };
        const присвоювач = описник.hasOwnProperty('set')
          ? описник.set
          : function (this: typeof обмета, зн: any) {
              Reflect.set(this, первинна, зн);
            };
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const { value, writable, ...підОписник } = описник;
        Object.defineProperty(обмета, переклад, {
          ...підОписник,
          get: отримувач,
          set: присвоювач,
        });
      }
    }
  }

  return обмета as ОчищеннаОбмета<T>;
}

declare global {
  // interface Array<T> {
  //   __розширений: true; // додаємо любе поле, щоб вивір не переводив intreface в type
  // }
  // interface ReadonlyArray<T> extends ОчищеніПоляОбмета<Readonly<TestArray<T>>> {
  //   __розширений: true; // додаємо любе поле, щоб вивір не переводив intreface в type
  // }
}

розширитиОчищенимиПолями(Array.prototype);

// type Test = { map: 3, readonly length: 3, split?: 3, readonly slice?: 3, mapMap: Test, readonly lengthMap: Test, splitMap?: Test, readonly sliceMap?: Test }
// const obj1: any = {
//   map: 3,
//   length: 3,
// }
// obj1.mapMap = obj1
// obj1.lengthMap = obj1

// const obj2: TestArray<number> = [1, 2]

// const a = розширитиОчищенимиПолями(obj2);

// a.length
// a.довжина

// type b = 567 extends number | 'a' | 'play' ? 1 : 2

// type sldkjg<T> = {
//   [K in keyof T]: T[K] extends 3 ? T[K] : never
// }

// type a = sldkjg<Test>
